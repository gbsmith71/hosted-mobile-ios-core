﻿using HostedMobile.iOS;
using HostedMobile;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.ComponentModel;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer(typeof(ExtendedTab), typeof(ExtendedTabRenderer))]
namespace HostedMobile.iOS
{
    public class ExtendedTabRenderer : TabbedRenderer
	{
		public static void Initialize()
		{
			// empty, but used for beating the linker
		}
	}
}

