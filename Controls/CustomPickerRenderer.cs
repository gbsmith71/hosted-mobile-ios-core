﻿using HostedMobile.iOS;
using HostedMobile;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.ComponentModel;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace HostedMobile.iOS
{
    public class CustomPickerRenderer : PickerRenderer
    {
        public static void Initialize()
        {
            // empty, but used for beating the linker
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Picker> e)
        {
            base.OnElementChanged(e);
            if (this.Control != null)
            {
                var customPicker = e.NewElement as CustomPicker;
                var downarrow = UIImage.FromBundle(customPicker.IconImage + "-" + customPicker.Theme +".png");
                var textField = this.Control;
                textField.TextAlignment = UITextAlignment.Center;
                textField.Font = UIFont.FromName(GlobalVariables.PickerFont, GlobalVariables.PickerFontSize);
				textField.RightViewMode = UITextFieldViewMode.Always;
                textField.RightView = new UIImageView(downarrow);
                textField.BorderStyle = UITextBorderStyle.None;
            }
        }
    }
}
