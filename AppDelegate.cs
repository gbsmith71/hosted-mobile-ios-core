﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using CarouselView.FormsPlugin.iOS;
using Foundation;
using UIKit;

namespace HostedMobile.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
			CarouselViewRenderer.Init();
			
            UITabBar.Appearance.SelectedImageTintColor = UIColor.FromRGB(GlobalVariables.TabColorSelectedRed, GlobalVariables.TabColorSelectedGreen, GlobalVariables.TabColorSelectedBlue); ;
            UITabBar.Appearance.BarTintColor = UIColor.FromRGB(GlobalVariables.TabColorTabBackgroundRed, GlobalVariables.TabColorTabBackgroundGreen, GlobalVariables.TabColorTabBackgroundBlue);
            UITabBar.Appearance.BackgroundColor = UIColor.FromRGB(GlobalVariables.TabColorTabBackgroundRed, GlobalVariables.TabColorTabBackgroundGreen, GlobalVariables.TabColorTabBackgroundBlue);

			var att = new UITextAttributes();
            att.Font = UIFont.FromName(GlobalVariables.NavigationFont, GlobalVariables.NavigationFontSize);
			UINavigationBar.Appearance.SetTitleTextAttributes(att);

            if (GlobalVariables.iOSStatusBarText == "light")
            {
                UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);
            } else {
                UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.BlackOpaque, false);
			}
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
    }
}
